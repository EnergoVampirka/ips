#include <iostream>
#include <iomanip>
#include <windows.h>
#include <cilk/cilk.h>
#include <cilk/reducer_opadd.h>
#include <math.h>
//#define ONLY_PARALLEL
#define _USE_MATH_DEFINES

double integral_function(double x)
{
	return (6/(sqrt(x*(2-x))));
}

double integral_parallel(const int N, const double b, const double a)
{
	const double step = (double) (b - a)/N;
	
	cilk::reducer_opadd<double> reduce;
	reduce.set_value(0);

	cilk_for(int i = 0; i < N - 1; ++i)
	{
		reduce += integral_function(a + (double)i * step);
	}

	double result = reduce.get_value() * step;
	return result;
}

double integral_usual(const int N, double b, double a)
{
	double step = (b - a)/N;
	double result = 0;

	for(int i = 0; i < N - 1; ++i)
	{
		result += integral_function(a + (double)i * step);
	}

	return result*step;
}

int main()
{
	# define M_PI 3.14159265358979323846 /* pi */

	//границы отрезка интегрирования
	const double b = 1;
	const double a = 0.5;
	//число разбиений отрезка
	const int N = 1000000000;
	//const int N = 10000;

	//функция подсчета времени
	DWORD time = 0;
	SYSTEMTIME start_time = {0};
	SYSTEMTIME stop_time = {0};

	double result = 0;

#ifdef ONLY_PARALLEL
	GetLocalTime(&start_time);
	//вычисление интеграла без IPS
	result = integral_usual(N, b, a);
	GetLocalTime(&stop_time);

	if (stop_time.wSecond > start_time.wSecond) 
	{
		time = (stop_time.wSecond - start_time.wSecond)*1000;
		time += stop_time.wMilliseconds;
		time -= start_time.wMilliseconds;
	}
	else
	{
		time = stop_time.wMilliseconds - start_time.wMilliseconds;
	}

	//вывод времени на экран
	std::cout << "Usual computing result: " << result << std::endl; 
	std::cout << "Elapsed time: "<< time << " ms." << std::endl << std::endl << std::endl;

#endif

	time = 0;
	result = 0;

	GetLocalTime(&start_time);
	//вычисление интеграла c IPS
	result = integral_parallel(N, b, a);
	GetLocalTime(&stop_time);

	if (stop_time.wSecond > start_time.wSecond) 
	{
		time = (stop_time.wSecond - start_time.wSecond) * 1000;
		time += stop_time.wMilliseconds;
		time -= start_time.wMilliseconds;
	}
	else
	{
		time = stop_time.wMilliseconds - start_time.wMilliseconds;
	}
	std::cout << std::setprecision(20);
	std::cout << "IPS computing result: " << result << std::endl; 
	std::cout << "Elapsed time: "<< time << " ms." << std::endl << std::endl << std::endl;

	double delta = M_PI - result;
	std::cout << std::setprecision(20);
	std::cout << "DELTA: " << delta << std::endl;

	getchar();
	return 0;
}